set guifont=Inconsolata\ 11
source $VIMRUNTIME/mswin.vim

set guioptions-=m " remove menu bar
set guioptions-=T " remove toolbar
nnoremap <C-F1> :if &go=~#'m'<Bar>set go-=m<Bar>else<Bar>set go+=m<Bar>endif<CR>
nnoremap <C-F2> :if &go=~#'T'<Bar>set go-=T<Bar>else<Bar>set go+=T<Bar>endif<CR>
