" add tex specific fold markers?
setlocal foldmethod=syntax
setlocal smartindent
let b:tex_flavor='latex'
let b:tex_stylish=1
let b:tex_conceal='admgs'
let b:tex_fold_enabled=1

source ~/.vim/undoPointOnPunctuation.vim
