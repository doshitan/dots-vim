" VIM Configuration File
" Complied from various sources, including:
" http://www.reddit.com/r/vim/comments/an9vo/what_vim_pluginscolorschemesetc_do_you_find_most/c0ifsfr
" http://redd.it/m2k76
" https://github.com/Pewpewarrows/dotfiles/blob/master/home/.vimrc
" https://github.com/remiprev/vimfiles
" https://github.com/bcat/dotfiles/blob/master/vimrc
" https://github.com/sjl/dotfiles/blob/master/vim/.vimrc
" https://github.com/skwp/dotfiles/blob/master/vim/plugin/settings/
" https://github.com/spf13/spf13-vim/blob/3.0/.vimrc
"
" TODO:
"   Get this file better organized/structured, maybe break it out

" Disable vi compatibility (emulation of old bugs)
set nocompatible

let mapleader = ","
let maplocalleader=","

" Filetype specific indention and plugins
filetype indent plugin on

" Set UTF-8 encoding
set encoding=utf-8
set fileencoding=utf-8
set termencoding=utf-8

" Enable mouse support.
if has("mouse")
  set mouse=a
  set mousefocus
  set mouseshape=n:beam,ve:beam,sd:updown
endif

" Remember EVERYTHING...
set history=1000
set undolevels=1000

" Use indentation of previous line
set autoindent

" Configure tabwidth and insert spaces instead of tabs
set tabstop=2      " tab width is 2 spaces
set shiftwidth=2   " indent also with 2 spaces
set softtabstop=2
set expandtab      " expand tabs to spaces
set smarttab
set shiftround     " round spaces to nearest shiftwidth multiple

" Appearance
set hidden      " hides buffers instead of closing them
set lazyredraw  " don't update the display while executing macros
set noshowmode  " hide the default mode text (e.g. -- INSERT -- below the statusline), no needed with default powerline
set showcmd
set visualbell  " no beeping
set cursorline  " highlight current line
set ttyfast     " fast terminal
set ruler       " tell us the current column/row
set showmatch   " highlight matching braces
set scrolloff=4 " gives the buffer some padding when scrolling
set nomodeline  " never commonly used, has history of security vulnerabilities

" Always show line numbers, but only in current window.
set number
:au WinEnter * :setlocal number
:au WinLeave * :setlocal nonumber

" Change to directory of file in buffer
set autochdir

set t_Co=256
syntax on

" Must be after loading theme, otherwise they are cleared
" TODO tweak the dark gray color to show up better (User2)
hi User1 ctermbg=236 ctermfg=7 guibg=#333333 guifg=#d3d7cf
hi User2 ctermbg=236 ctermfg=8 guibg=#333333 guifg=#555555

set laststatus=2  " always displays the status line for consistency
function! GetCWD()
  return expand(":pwd")
endfunction

function! IsHelp()
  return &buftype=='help'?' (help) ':''
endfunction

function! GetName()
  return expand("%:t")==''?'<none>':expand("%:t")
endfunction

set statusline=%2*[%1*%{GetName()}%2*]%<%1*
set statusline+=%{&modified?'\ (modified)':''}%1*
set statusline+=%{IsHelp()}
set statusline+=%{&readonly?'\ (read-only)':''}%1*
set statusline+=\ %2*fenc:%1*%{strlen(&fenc)?&fenc:'none'}
set statusline+=\ %2*ff:%1*%{&ff}
set statusline+=\ %2*ft:%1*%{strlen(&ft)?&ft:'<none>'}
set statusline+=\ %2*tab:%1*%{&ts}
set statusline+=%2*,%1*%{&sts}
set statusline+=%2*,%1*%{&sw}
set statusline+=%2*,%1*%{&et?'et':'noet'}
set statusline+=\ %<%2*pwd:%1*%{getcwd()}
set statusline+=%1*%=
set statusline+=%2{SyntasticStatuslineFlag()}%1*
set statusline+=\ %2*col:%1*%c
set statusline+=\ %2*line:%1*%l
set statusline+=\ %2*total:%1*%L

set title " display filename in window title

" Highlight VCS conflict markers
match ErrorMsg '^\(<\|=\|>\)\{7\}\([^=].\+\)\?$'

" Macro for navigating through a merge conflict
let @a='/^(\<|\=|\>){7}'

" Highlight trailing whitespace
match Todo /\(\t\|\s\)\+$/

set listchars=tab:▸\ ,trail:·,extends:#,nbsp:·,eol:¬
set showbreak=↪
set nolist " don't show invisible characters by default, only for some files
noremap <leader>i :set list!<CR> " Toggle invisible chars

" Better command-line completion
set wildmenu
set wildmode=list:longest
set wildignore=*.swp,*.bak,*.pyc,*.class

" Editing Behavior
set backspace=indent,eol,start " backspace over everything in insert mode
set fileformats="unix,dos,mac"
set wrap
set textwidth=78
set formatoptions=qrnl1tc " better line-wrapping, see :help fo-table
if v:version >= 703
  set colorcolumn=78
endif
set autoread

set ignorecase
set smartcase
set incsearch
set gdefault " search/replace is globally done on a line by default
set hlsearch

set viminfo=!,'100,<1000,s100,h,n~/.vim-local/viminfo

" Remember cursor position
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g`\"" | endif

" Don't pollute the current working directory with this nonsense
set backup
"set swapfile
set noswapfile
set backupdir=~/.vim-local/backup,/tmp
set directory=~/.vim-local/swap,/tmp
if v:version >= 703
  set undofile
  set undodir=~/.vim-local/undo,/tmp
endif

" Spell settings
" toggle spell checking on and off with `,s`
set spell
nmap <silent> <leader>s :set spell!<CR>
set spelllang=en_us
map <M-Down> ]s
map <M-Up> [s

" OmniCompletion
set omnifunc=syntaxcomplete#Complete

" Completion Settings
set completeopt=menu,menuone,longest,preview " Complete options (disable preview scratch window)
set pumheight=15 " Limit popup menu height

" Folding
set foldmethod=indent
set foldnestmax=2
set foldopen=block,hor,insert,jump,mark,percent,quickfix,search,tag,undo
nnoremap <silent> <Space> @=(foldlevel('.')?'za':"\<Space>")<CR>
vnoremap <Space> zf

" Allow concealing
set conceallevel=2

" Enhanced keyboard mappings
" -----------------------------------------------------------------------------
" save the pinky
nnoremap ; :
inoremap jj <Esc>
" line movement changed to what you see on screen, not literal file lines
nnoremap <silent> k gk
nnoremap <silent> j gj
inoremap <silent> <Up> <Esc>gka
inoremap <silent> <Down> <Esc>gja
" in normal mode F2 will save the file
nmap <F2> :w<CR>
" in insert mode F2 will exit insert, save, enters insert again
imap <F2> <ESC>:w<CR>a
" toggle paste and show paste status in normal mode
nnoremap <F3> :set paste! paste?<CR>
" combined with 'set showmode', this will show paste status in insert mode
set pastetoggle=<F3>
" switch between header/source with F4
"map <F4> :e %:p:s,.h$,.X123X,:s,.cpp$,.h,:s,.X123X$,.cpp,<CR>
" uses FSwitch plugin
map <F4> :FSHere<CR>
" recreate tags file with F5
map <F5> :!ctags -R -c++-kinds=+p -fields=+iaS -extra=+q .<CR>
" build using makeprg with <F7>
map <F7> :make<CR>
" build using makeprg with <S-F7>
map <S-F7> :make clean all<CR>
" goto definition with F12
map <F12> <C-]>
" quickly edit and reload the .vimrc file
nmap <silent> <leader>ev :e ~/.vimrc<CR>
nmap <silent> <leader>sv :so ~/.vimrc<CR>
" prevent accidental call for help
inoremap <F1> <ESC>
nnoremap <F1> <ESC>
vnoremap <F1> <ESC>
" use Perl-style regex
nnoremap / /\v
vnoremap / /\v
" Center the line that the search result is on
map N Nzz
map n nzz
map tn :tabnew<CR>
map td :tabclose<CR>
map tr :tabnext<CR> " tab right
map tl :tabprevious<CR> " tab left
" Improved splits movement
nnoremap <leader>w <C-w>v<C-w>l
nnoremap <leader>W <C-w>s<C-w>j
noremap <C-h> <C-w>h
noremap <C-j> <C-w>j
noremap <C-k> <C-w>k
noremap <C-l> <C-w>l

" Create window splits easier. The default
" way is Ctrl-w,v and Ctrl-w,s.
nnoremap <silent> vv <C-w>v
nnoremap <silent> ss <C-w>s
" Split and move, same as the ,w and ,W above, pick one
nnoremap <silent> vvm <C-w>v<C-w>l
nnoremap <silent> ssm <C-w>s<C-w>j

" alias yw to yank the entire word 'yank inner word'
" even if the cursor is halfway inside the word
" FIXME: will not properly repeat when you use a dot (tie into repeat.vim)
nnoremap <leader>yw yiww

" ,ow = 'overwrite word', replace a word with what's in the yank buffer
" FIXME: will not properly repeat when you use a dot (tie into repeat.vim)
nnoremap <leader>ow "_diwhp

"make Y consistent with C and D
nnoremap Y y$

" ========================================
" RSI Prevention - keyboard remaps
" ========================================
" Certain things we do every day as programmers stress
" out our hands. For example, typing underscores and
" dashes are very common, and in position that require
" a lot of hand movement. Vim to the rescue
"
" Now using the middle finger of either hand you can type
" underscores with apple-k or apple-d, and add Shift
" to type dashes
"imap <silent> <D-k> _
"imap <silent> <D-d> _
"imap <silent> <D-K> -
"imap <silent> <D-D> -

" ,# Surround a word with #{ruby interpolation}
map <leader># ysiw#
vmap <leader># c#{<C-R>"}<ESC>

" ," Surround a word with "quotes"
map <leader>" ysiw"
vmap <leader>" c"<C-R>""<ESC>

" ,' Surround a word with 'single quotes'
map <leader>' ysiw'
vmap <leader>' c'<C-R>"'<ESC>

" ,) or ,( Surround a word with (parens)
" The difference is in whether a space is put in
map <leader>( ysiw(
map <leader>) ysiw)
vmap <leader>( c( <C-R>" )<ESC>
vmap <leader>) c(<C-R>")<ESC>

" ,[ Surround a word with [brackets]
map <leader>] ysiw]
map <leader>[ ysiw[
vmap <leader>[ c[ <C-R>" ]<ESC>
vmap <leader>] c[<C-R>"]<ESC>

" ,{ Surround a word with {braces}
map <leader>} ysiw}
map <leader>{ ysiw{
vmap <leader>} c{ <C-R>" }<ESC>
vmap <leader>{ c{<C-R>"}<ESC>

" Change inside various enclosures with Cmd-" and Cmd-'
" The f makes it find the enclosure so you don't have
" to be standing inside it
nnoremap <D-'> f'ci'
nnoremap <D-"> f"ci"
nnoremap <D-(> f(ci(
nnoremap <D-)> f)ci)
nnoremap <D-[> f[ci[
nnoremap <D-]> f]ci]

"Go to last edit location with ,.
nnoremap <leader>. `.

" ============================
" Shortcuts for everyday tasks
" ============================

" copy current filename into system clipboard - mnemonic: (c)urrent(f)ilename
" this is helpful to paste someone the path you're looking at
nnoremap <silent> ,cf :let @* = expand("%:~")<CR>
nnoremap <silent> ,cn :let @* = expand("%:t")<CR>

"Clear current search highlight by double tapping //
nmap <silent> // :nohlsearch<CR>

"(v)im (c)ommand - execute current line as a vim command
nmap <silent> ,vc yy:<C-f>p<C-c><CR>

"(v)im (r)eload
nmap <silent> ,vr :so %<CR>

" Type ,hl to toggle highlighting on/off, and show current value.
noremap <leader>hl :set hlsearch! hlsearch?<CR>
map <silent> <C-N> :set hlsearch!<CR> " toggle highlight with CTRL-N

" Highlight all occurrences of current word (like '*' but without moving)
" http://vim.wikia.com/wiki/Highlight_all_search_pattern_matches
nnoremap <silent> <leader>* :let @/='\<<C-R>=expand("<cword>")<CR>\>'<CR>:set hls<CR>

" These are very similar keys. Typing 'a will jump to the line in the current
" file marked with ma. However, `a will jump to the line and column marked
" with ma. It’s more useful in any case I can imagine, but it’s located way
" off in the corner of the keyboard. The best way to handle this is just to
" swap them: http://items.sjbach.com/319/configuring-vim-right
nnoremap ' `
nnoremap ` '

" via: http://rails-bestpractices.com/posts/60-remove-trailing-whitespace
" Strip trailing whitespace
function! <SID>StripTrailingWhitespaces()
    " Preparation: save last search, and cursor position.
    let _s=@/
    let l = line(".")
    let c = col(".")
    " Do the business:
    %s/\s\+$//e
    " Clean up: restore previous search history, and cursor position
    let @/=_s
    call cursor(l, c)
endfunction
command! StripTrailingWhitespaces call <SID>StripTrailingWhitespaces()
nmap <leader>sw :StripTrailingWhitespaces<CR>
" Alternatively, maybe just:
" autocmd BufWritePre * :%s/\s\+$//e

" w!! to write a file as sudo
" stolen from Steve Losh
cmap w!! w !sudo tee % >/dev/null

" Stolen from Steve Losh vimrc: https://bitbucket.org/sjl/dotfiles/src/tip/vim/.vimrc
" Open a Quickfix window for the last search.
nnoremap <silent> <leader>q/ :execute 'vimgrep /'.@/.'/g %'<CR>:copen<CR>

" Ack for the last search.
nnoremap <silent> <leader>qa/ :execute "Ack! '" . substitute(substitute(substitute(@/, "\\\\<", "\\\\b", ""), "\\\\>", "\\\\b", ""), "\\\\v", "", "") . "'"<CR>

" GitGrep the last search (my own invention)
nnoremap <silent> <leader>qg/ :execute "GitGrep '" . substitute(substitute(substitute(@/, "\\\\<", "\\\\b", ""), "\\\\>", "\\\\b", ""), "\\\\v", "", "") . "'"<CR>

nmap <leader>u :GundoToggle<CR>

nmap <leader>t :NERDTreeToggle<CR>

nmap <leader>f :CtrlPMixed<CR>
nmap <leader>a :Ack<space>

" http://vim.wikia.com/wiki/Toggle_to_open_or_close_the_quickfix_window
function! GetBufferList()
  redir =>buflist
  silent! ls
  redir END
  return buflist
endfunction

function! ToggleList(bufname, pfx)
  let buflist = GetBufferList()
  for bufnum in map(filter(split(buflist, '\n'), 'v:val =~ "'.a:bufname.'"'), 'str2nr(matchstr(v:val, "\\d\\+"))')
    if bufwinnr(bufnum) != -1
      exec(a:pfx.'close')
      return
    endif
  endfor
  if a:pfx == 'l' && len(getloclist(0)) == 0
      echohl ErrorMsg
      echo "Location List is Empty."
      return
  endif
  let winnr = winnr()
  exec(a:pfx.'open')
  if winnr() != winnr
    wincmd p
  endif
endfunction

nmap <silent> <leader>l :call ToggleList("Location List", 'l')<CR>
nmap <silent> <leader>q :call ToggleList("Quickfix List", 'c')<CR>

nmap <silent> <leader>lc :lclose<CR>
nmap <silent> <leader>lo :lopen<CR>
nmap <silent> <leader>ln :lnext<CR>
nmap <silent> <leader>lp :lprevious<CR>
nmap <silent> <leader>qc :cclose<CR>
nmap <silent> <leader>qo :copen<CR>
nmap <silent> <leader>qn :cnext<CR>
nmap <silent> <leader>qp :cprevious<CR>


" Plugins
" -----------------------------------------------------------------------------

" vim-addon-manager, the minimal self-installing version, a well commented
" version is available in VAM docs
function! SetupVAM()
  let c = get(g:, 'vim_addon_manager', {})
  let g:vim_addon_manager = c
  let c.plugin_root_dir = expand('$HOME', 1) . '/.vim/vim-addons'
  let &rtp.=(empty(&rtp)?'':',').c.plugin_root_dir.'/vim-addon-manager'
  " let g:vim_addon_manager = { your config here see "commented version" example and help
  if !isdirectory(c.plugin_root_dir.'/vim-addon-manager/autoload')
    execute '!git clone --depth=1 git://github.com/MarcWeber/vim-addon-manager '
                \ shellescape(c.plugin_root_dir.'/vim-addon-manager', 1)
  endif
  call vam#ActivateAddons(['UltiSnips'
                         \,'LaTeX_Box'
                         \,'delimitMate'
                         \,'The_NERD_Commenter'
                         \,'fugitive'
                         \,'Syntastic'
                         \,'Tagbar'
                         \,'EasyMotion'
                         \,'surround'
                         \,'textobj-function'
                         \,'textobj-indent'
                         \,'arpeggio'
                         \,'neocomplcache'
                         \,'Tabular'
                         \,'ctrlp'
                         \,'bufexplorer.zip'
                         \,'vim-signify'
                         \,'haskellmode-vim'
                         \,'ghcmod'
                         \,'neco-ghc'
                         \,'github:JazzCore/neocomplcache-ultisnips'
                         \,'FSwitch'
                         \,'vim-less'
                         \,'Haskell_Conceal'
                         \,'Gundo'
                         \,'html-template-syntax'
                         \,'github:urso/haskell_syntax.vim'
                         \,'ack'
                         \,'github:osyo-manga/vim-over'
                         \,'github:christoomey/vim-tmux-navigator'
                         \,'vim-airline'
                         \,'The_NERD_tree'
                         \,'vim-multiple-cursors'
                         \,'github:jnurmine/Zenburn'
                         \,'JSON'
                         \], {'auto_install' : 1})
endfunction
call SetupVAM()

" NERD Commenter
let NERDSpaceDelims=1 " put spaces around comment delimiters
let NERD_haskell_alt_style=1 " prefer -- over {- -}

" airline
let g:airline_powerline_fonts = 1

" ctrlp
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlPMixed'

" SuperTab
let g:SuperTabDefaultCompletionType = 'context' " context aware completion
"function LatexContext()
  "return "\<c-x>\<c-o>"
"endfunction
"let g:SuperTabCompletionContexts = ['LatexContext', 's:ContextText']

" Latex Box
let g:LatexBox_latexmk_options = '-pvc'

" delimitMate
"let delimitMate_excluded_ft = "tex,mail,txt"
let g:delimitMate_expand_cr = 1
let g:delimitMate_expand_space = 1

" UltiSnips
"let g:UltiSnipsExpandTrigger="<tab>"
"let g:UltiSnipsListSnippets="<c-tab>"
"let g:UltiSnipsJumpForwardTrigger="<tab>"
"let g:UltiSnipsJumpBackwardTrigger="<s-tab>"
"let g:UltiSnipsExpandTrigger="<C-space>"
"let g:UltiSnipsListSnippets="<C-r>"
"inoremap <expr> <c-r> UltiSnips_ListSnippets()

" Syntastic
let g:syntastic_mode_map = { 'mode': 'active',
                           \ 'active_filetypes': [],
                           \ 'passive_filetypes': ['javascript'] }
let g:syntastic_haskell_checkers=['ghc_mod', 'hlint']


" Tagbar
nmap <F8> :TagbarToggle<CR>

" fugitive
" " For fugitive.git, dp means :diffput. Define dg to mean :diffget
nnoremap <silent> <leader>dg :diffget<CR>
nnoremap <silent> <leader>dp :diffput<CR>

" neocomplcache
" haskellmode-vim sets completefunc, without a way to disable it. So force
" overwrite it because we don't want it's autocomplete stuff
let g:neocomplcache_force_overwrite_completefunc = 1
let g:neocomplcache_enable_at_startup = 1
let g:neocomplcache_enable_smart_case = 1
let g:neocomplcache_enable_camel_case_completion = 1
let g:neocomplcache_enable_underbar_completion = 1
"let g:neocomplcache_enable_auto_select = 1
" Recommended key-mappings.
" <CR>: close popup and save indent.
inoremap <expr><CR>  neocomplcache#smart_close_popup() . "\<CR>"
" <TAB>: completion.
inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"
" <C-h>, <BS>: close popup and delete backword char.
"inoremap <expr><C-h> neocomplcache#smart_close_popup()."\<C-h>"
"inoremap <expr><BS> neocomplcache#smart_close_popup()."\<C-h>"
"inoremap <expr><C-y>  neocomplcache#close_popup()
"inoremap <expr><C-e>  neocomplcache#cancel_popup()

 " Tabular {
  nmap <Leader>a& :Tabularize /&<CR>
  vmap <Leader>a& :Tabularize /&<CR>
  nmap <Leader>a= :Tabularize /=<CR>
  vmap <Leader>a= :Tabularize /=<CR>
  nmap <Leader>a: :Tabularize /:<CR>
  vmap <Leader>a: :Tabularize /:<CR>
  nmap <Leader>a:: :Tabularize /:\zs<CR>
  vmap <Leader>a:: :Tabularize /:\zs<CR>
  nmap <Leader>a, :Tabularize /,<CR>
  vmap <Leader>a, :Tabularize /,<CR>
  nmap <Leader>a<Bar> :Tabularize /<Bar><CR>
  vmap <Leader>a<Bar> :Tabularize /<Bar><CR>
" }

" Haskell mode
let g:haddock_browser = "xdg-open"

" Put colorscheme last so the rtp is set to look at for colors in vim-addons
colorscheme zenburn

if filereadable(expand("~/.vim-local/vimrc"))
  source ~/.vim-local/vimrc
endif
